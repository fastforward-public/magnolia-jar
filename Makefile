# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help

build-postgres: ## Build docker images.
	docker build . -t magnolia-jar:postgres-42.2.8 \
		--build-arg MVN_GROUP_ID=org.postgresql \
		--build-arg MVN_ARTIFACT_ID=postgresql \
		--build-arg MVN_VERSION=42.2.8 \
		--build-arg MVN_DEST=/jars/postgresql.jar

build-jmxexporter: ## Build docker images.
	docker build . -t magnolia-jar:jmxexporter-0.13.0 \
		--build-arg MVN_GROUP_ID=io.prometheus.jmx \
		--build-arg MVN_ARTIFACT_ID=jmx_prometheus_javaagent \
		--build-arg MVN_VERSION=0.13.0 \
		--build-arg MVN_DEST=/jars/jmx_prometheus_javaagent-0.13.0.jar

build-cloud-bootstrap: ## Build docker images.
	docker build . -t magnolia-jar:magnolia-cloud-bootstrapper-1.0 \
		--build-arg MVN_GROUP_ID=info.magnolia.cloud \
		--build-arg MVN_ARTIFACT_ID=magnolia-cloud-bootstrapper \
		--build-arg MVN_VERSION=1.0 \
		--build-arg MVN_REPO_URL=https://nexus.magnolia-cms.com/content/groups/public \
		--build-arg MVN_DEST=/jars/magnolia-cloud-bootstrapper-1.0.jar
