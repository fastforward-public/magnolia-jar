# Magnolia JAR Repo

This is a collection of Docker images containing libraries for use in the [Magnolia Helm chart](https://gitlab.com/mironet/magnolia-helm).

## How these are built

Because fetching libraries from Maven is painful without having Maven installed (even with Maven it's not trivial) we use the Ansible [maven_artifact](https://docs.ansible.com/ansible/latest/modules/maven_artifact_module.html) module.

In the Docker container we first install ansible and then fetch the appropriate libraries by specifying these environment variables in the [CI job configuration](.gitlab-ci.yml).

```yaml
    - export DOCKER_IMAGE=$CI_REGISTRY_IMAGE:jmx_prometheus_javaagent-0.13.0
    - export MVN_GROUP_ID=io.prometheus.jmx
    - export MVN_ARTIFACT_ID=jmx_prometheus_javaagent
    - export MVN_VERSION=0.13.0
    - export MVN_DEST_NAME=jmx_prometheus_javaagent.jar
```

After fetching the library it is packaged up with an [init.sh](init.sh) script which is used in an `initContainer` when starting up Magnolia to copy the jar to the appropriate location inside the running Tomcat instance.

The resulting images are then available in the Docker registry tagged by the `DOCKER_IMAGE` variable set before.
